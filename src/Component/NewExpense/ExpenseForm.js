import React, { useState } from 'react'
import './ExpenseForm.css'

function ExpenseForm(props) {
    const [userInput, setUserInput] = useState({
        enteredTitle: '',
        enteredAmount: '',
        enteredDate: '',
    })
    const titleChangeHandler = (event) => {
        setUserInput({
            ...userInput,
            enteredTitle: event.target.value,
        })

    }
    const amountChangeHanldler = (event) => {
        setUserInput({
            ...userInput,
            enteredAmount: event.target.value,
        })
    }
    const dateChangeHanldler = (event) => {
        setUserInput({
            ...userInput,
            enteredDate: event.target.value,
        })
    }
    const addExpenseHandler = (event) => {
        event.preventDefault()
        const expenseData = {
            title: userInput.enteredTitle,
            amount: userInput.enteredAmount,
            date: new Date(userInput.enteredDate),
        }
        props.onSaveExpenseData(expenseData)
        setUserInput({
            ...userInput,
            enteredAmount: '',
            enteredTitle: '',
            enteredDate: '',
        })
        // console.log(expenseData);

    }
    return (
        <form onSubmit={addExpenseHandler}>
            <div className='new-expense__controls'>
                <div className='new-expense_control'>
                    <lable>Tilte</lable>
                    <input type='text' value={userInput.enteredTitle} onChange={titleChangeHandler} />
                </div>
                <div className="new-expense__controls">
                    <label>Amount</label>
                    <input type='number' min='0.01' step='0.01' value={userInput.enteredAmount} onChange={amountChangeHanldler} />
                </div>
                <div className="new-expense__controls">
                    <label>Date</label>
                    <input type='date' min='2019-01-01' step='2012-12-31' value={userInput.enteredDate} onChange={dateChangeHanldler} />
                </div>
                <div className="new-expense__control">
                    <button type="button" onClick={props.onCancel}>Cancel</button>
                    <button type='submit'>Add Expense</button>
                </div>
            </div>
        </form>
    )
}

export default ExpenseForm