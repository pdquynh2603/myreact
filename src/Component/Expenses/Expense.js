import React, { useState } from 'react'
import Expenseltem from "./ExpenseItem"
import './Expense.css'
import ExpenseFilter from "./ExpenseFilter"
import Card from '../UI/Card'
import ExpenseList from './ExpensesList'

function Expense(props) {
    const expenses = [
        {
            id: 'el',
            title: 'Toilet Paper',
            amount: 94.12,
            date: new Date(2020, 7, 14)
        },
        {
            id: 'e2',
            title: 'New TV',
            amount: 799.49,
            date: new Date(2020, 2, 12)
        },
        {
            id: 'e3',
            title: 'Car Insurance',
            amount: 294.67,
            date: new Date(2020, 2, 28)
        }
    ]
    const [filterYear, setFilterYear] = useState('2020')
    const filterChangeHandler = selectedYear => {
        setFilterYear(selectedYear);
    }

    const filteredExpenses = props.items.filter(expense => {
        return expense.date.getFullYear().toString() === filterYear
    })

    return (
        <div>

            <Card className='expenses'>
                <ExpenseFilter selecterd={filterYear} onChangeFilter={filterChangeHandler} />


                <ExpenseList items={filteredExpenses} />
            </Card>

        </div>
    )

}

export default Expense