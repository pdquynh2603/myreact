import Expense from './Component/Expenses/Expense'
import './App.css';
import NewExpense from './Component/NewExpense/NewExpense';
import { useState } from 'react';

const DUMMY_EXPENSE = [
  {
    id: 'el',
    title: 'Toilet Paper',
    amount: 94.12,
    date: new Date(2020, 7, 14)
  },
  {
    id: 'e2',
    title: 'New TV',
    amount: 799.49,
    date: new Date(2020, 2, 12)
  },
  {
    id: 'e3',
    title: 'Car Insurance',
    amount: 294.67,
    date: new Date(2020, 2, 28)
  }
]

const App = () => {
  const [expenses, setExpense] = useState(DUMMY_EXPENSE)
  const addExpense = (expense) => {
    setExpense((prevExpenses) => {
      return [expense, ...prevExpenses]
    })
  }

  return (
    <div>
      <NewExpense onAddExpense={addExpense} />
      <Expense items={expenses} />
    </div>
  );
}

export default App;
